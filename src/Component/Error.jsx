import React, { Component } from "react";
import { NavLink } from "react-router-dom";

export default class Error extends Component {
  render() {
    return (
      <div>
        <img
          src="./44-incredible-404-error-pages@3x-1560x760.webp"
          alt="error"
        />
      </div>
    );
  }
}
