import React from "react";
import { NavLink } from "react-router-dom";

function Home() {
  return (
    <div className=" h-screen float-right bg-teal-100 w-4/5">
      <h3 className=" ml-10 mt-4">🚀 Welcome To Home Page.</h3>
      <ul className=" mt-6 text-center">
        <NavLink to="/articles">
          <li className=" underline text-2xl pt-3 ml-12 inline-block bg-white w-72 h-16 mr-6 rounded shadow-slate-400 shadow-xl">
            Articale Pages
          </li>
        </NavLink>
      </ul>
    </div>
  );
}

export default Home;
