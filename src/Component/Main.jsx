import React from "react";
import Top from "./ChildComponent/Top";
import Side from "./ChildComponent/Side";
import { Outlet } from "react-router-dom";

function Main() {
  return (
    <>
      <Top />
      <div className="inline-block w-1/5">
        <Side />
      </div>
      <Outlet />
    </>
  );
}

export default Main;
