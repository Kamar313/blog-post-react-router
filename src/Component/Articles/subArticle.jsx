import React from "react";
import { useParams } from "react-router-dom";
import { NavLink } from "react-router-dom";

function SubArticle() {
  const peram = useParams();
  const { name } = peram;
  return (
    <div className=" h-screen float-right bg-teal-100 w-4/5 ">
      <NavLink to="/articles">
        <h3 className="w-48  text-center underline">👈 Go Back To Article</h3>
      </NavLink>
      {`The slug of the article is ::  ${name}`}
    </div>
  );
}

export default SubArticle;
