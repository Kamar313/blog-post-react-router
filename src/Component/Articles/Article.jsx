import data from "../Data/data";
import React, { Component } from "react";
import { NavLink } from "react-router-dom";
export default class Article extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filterArr: data,
    };
  }

  onchangeInputValue = (e) => {
    let filterData = data.filter((ele) => {
      return ele.title
        .toLocaleLowerCase()
        .includes(e.target.value.toLocaleLowerCase());
    });
    this.setState({ filterArr: filterData });
  };

  render() {
    return (
      <div id="article" className=" h-screen float-right bg-teal-100 w-4/5">
        <input
          type="text"
          className=" w-2/5 h-10 rounded ml-10 mt-3 mb-3"
          onChange={this.onchangeInputValue}
        ></input>
        {this.state.filterArr.map((ele) => {
          return (
            <div className=" ml-10 mb-4">
              <NavLink to={`/subarticle/${ele.slug}`}>
                <h3 className=" underline text-lg visited:text-purple-600">
                  {ele.title}
                </h3>
              </NavLink>
              <p>{ele.author}</p>
            </div>
          );
        })}
      </div>
    );
  }
}
