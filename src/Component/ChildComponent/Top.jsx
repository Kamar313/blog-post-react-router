import React from "react";
import { Link } from "react-router-dom";
function Top() {
  return (
    <div className=" bg-gradient-to-r from-indigo-500 h-16 w-1/1 flex items-center">
      <Link to="/">
        <h2 className=" text-2xl text-white ml-16">Dashboard</h2>
      </Link>
    </div>
  );
}

export default Top;
