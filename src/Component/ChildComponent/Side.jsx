import React from "react";
import { NavLink } from "react-router-dom";
import "./side.css";

function Side() {
  return (
    <div>
      <ul className="pl-10">
        <li className=" h-16 mt-4 text-xl w-1/1">
          <NavLink to="/"> Home </NavLink>
        </li>
        <li className=" h-16 mt-4 text-xl w-1/1">
          <NavLink to="/articles">Articles</NavLink>
        </li>
      </ul>
    </div>
  );
}

export default Side;
