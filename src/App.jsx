import { BrowserRouter, Routes, Route } from "react-router-dom";
import React, { Component } from "react";
import Main from "./Component/Main";
import Home from "./Component/Home/Home";
import Article from "./Component/Articles/Article";
import SubArticle from "./Component/Articles/subArticle";
import Error from "./Component/Error";

export default class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Main />}>
            <Route path="/" element={<Home />} />
            <Route path="/articles" element={<Article />} />
            <Route path="/subarticle/:name" element={<SubArticle />} />
          </Route>
          <Route path="*" element={<Error />} />
        </Routes>
      </BrowserRouter>
    );
  }
}
